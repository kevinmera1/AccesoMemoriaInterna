package uleam.facci.kevinmera.accesomemoriainterna;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import static android.provider.Telephony.Mms.Part.FILENAME;

public class MainActivity extends AppCompatActivity {

    Button guardar, leer;
    EditText archivo, mensaje;
    TextView mostrar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        guardar = (Button)findViewById(R.id.buttonGuardar);
        leer = (Button)findViewById(R.id.buttonLeer);
        archivo = (EditText)findViewById(R.id.editTextArchivo);
        mensaje = (EditText)findViewById(R.id.editTextMensaje);
        mostrar = (TextView)findViewById(R.id.textViewMostrar);

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    OutputStreamWriter out = new OutputStreamWriter(openFileOutput(archivo.getText().toString()+".txt", Context.MODE_PRIVATE));
                    out.write(mensaje.getText().toString());
                    out.close();
                }catch (Exception ex){
                    Log.e("Archivo","Error al guardar en la memoria interna");
                }
            }
        });

        leer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    BufferedReader read = new BufferedReader(new InputStreamReader(openFileInput(archivo.getText().toString()+".txt")));
                    String texto = read.readLine();
                    mostrar.setText(texto);
                    read.close();
                }catch (Exception ex){
                    Log.e("Archivo","Error al leer el archivo de la memoria interna");
                }
            }
        });


    }

}
